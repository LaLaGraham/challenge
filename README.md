Laura LaRocca Challenge README
laglarocca@gmail.com

This project is in ReadyAPI | SOAPUI Pro.  If you don't already have that tool, you can download a demo version that will be good for a 

month.

To see the project, open ReadyAPI | SOAPUI Pro, then Import Project.

The test can be run at either the Project, Test Suite, or Test Case.  There is a loop in the Test Case, so the Test Case is a unit and 

cannot be run at the Test Step level.

There are 8 functional tests included in the loop and 2 error handling tests following the loop.  The Assertions are built in, so if 

the Test Case displays as Green, they have Passed.  The names of the Tests are included in the Data Loop or the Request Name.